# Raspberry Pi

This repository is used for documentation, issue tracking, and project management for the Raspberry Pi port of Ubuntu Touch. Unlike the other Community Ports repositories, it does not contain binaries for building system images.

**Devices**

**Fully supported devices**

At the moment there are no fully supported devices. That does not mean you can't try Ubuntu Touch on it. The Raspberry Pi 3 and 4 can boot and development can start from there.
The software is currently pulled from Ubuntu Touch's Edge release channel plus some changes for Wayland. This means that it runs on the latest software and could be broken once in a while due to ongoing development.

**Development devices**

Raspberry Pi 3 model B: a Broadcom BCM2837 SoC with a 1.2 GHz 64-bit quad-core ARM Cortex-A53 processor, with 512 KB shared L2 cache. With an internal memory of 1 GB of RAM.
Raspberry Pi 3 model B+: a Broadcom BCM2837 SoC with a 1.4 GHz 64-bit quad-core ARM Cortex-A53 processor, with 512 KB shared L2 cache. With an internal memory of 1 GB of RAM.
Raspberry Pi 4 model B: a Broadcom BCM2711 SoC with a 1.5 GHz 64-bit quad-core ARM Cortex-A72 processor, with 1MB shared L2 cache. With an internal memory of 1, 2 or 4 GB of RAM.

Please take note that 2 GB of RAM or more is preferred to have the best experience.
The following devices lack performance so will not give a good user experience

Raspberry Pi 1 and 2: are not under development because they lack performance based on processor power and internal memory.
Raspberry Pi 3 model A+: a Broadcom BCM2837 SoC with a 1.4 GHz 64-bit quad-core ARM Cortex-A53 processor, with 512 KB shared L2 cache. With an internal memory of 512 MB of RAM.


## How do I install Ubuntu Touch on my Raspberry Pi?

**Requirements:**

Raspberry Pi, micro sd card reader, 4-32 GB micro sd card.

**Installation procedure:**

Go to [the latest build](https://ci.ubports.com/job/rootfs/job/rootfs-rpi/) and download ubuntu-touch-raspberrypi.img.gz 
The image is compressed so needs to be unzipped. You should be able to do that in your file manager. Navigate to the Downloads folder (or whichever folder you used), identify the file, right click on it and choose extract from the options. It will take a little time to extract the 4GB image.

Writing the SD card  can be done via the Terminal. Put the card in the card reader. Open Terminal and type in $ sudo fdisk -l to find the card. In my case this showed that /dev/sda  was the 32GB card. To write the image to the card simply change directory to where you saved the file (in my case typing $ cd Downloads in the terminal got me there) then, still in the terminal, type in $ sudo dd bs=4M if=ubuntu-touch-raspberrypi.img of=/dev/sda conv=fsync
* Please replace /dev/sda with the correct path to your SD card.

Running DD on a 4GB file will take several minutes. Once it has finished you can close the terminal.

Alternatively download Balena Etcher, extract the zip file and make sure it is executable (chmod +x balenaEtcher-xxxxxx.AppImage for example).
Run Balena Etcher and select ubuntu-touch-raspberrypi.img.gz, select your sd card and flash it.

Insert your micro SD card into the slot on the rear of the Raspberry Pi.
Make sure to have plugged in your keyboard and boot your Raspberry Pi.

Type the default password:phablet (phablet is the default login and password).
You have booted Ubuntu Touch into the home screen. If you have Ethernet connection it is easier to get through the setup screens.


**What works, what doesn't?**

**Working**

Boot into GUI (only on the Raspberry 3 the Pi 4 has unfortunately graphical problems at the moment).
Keyboard and mouse (some odd selection issues but greatly improved since there was a fix to the mouse hover event bug).
Openstore.
Touchscreen (swipe from edge does not work at present).
Wired network.
WiFi.
Log Viewer - handy for finding where an issue is and sharing via pastebin.

**Not working**

Sound (only working in the command line at present).
Bluetooth.
Some older apps that are not yet compliant with Edge.
Moving files within the UI and pasting files between apps (partially working).


## How is the Raspberry Pi image built?

Unlike most supported devices, the Raspberry Pi does not require the use of Android drivers or services. Therefore, building Halium for this device is not required. Instead, we build the images for the Raspberry Pi using the debos configuration found in [ubports/core/rootfs-builder-debos](https://gitlab.com/ubports/core/rootfs-builder-debos) on GitLab. See the raspberrypi.yaml file for the configuration and documentation, see the repository's readme.md file for information on building the images yourself.
The lack of Android also causes its own problems, which are tracked via the Issues page of this repository.
Additionally, we use the Mesa drivers to run on the Raspberry Pi. The Mir backend for Mesa does not support Mir-on-Mir, which is used by all other supported devices to run applications. Applications running on the Raspberry Pi use the Wayland protocol, not MirAL, to speak to Mir. Therefore, all of the issues posted in the Waylandify project on UBports' GitHub affect Ubuntu Touch on the Raspberry Pi.


## How does this repository work?

We track issues which are specific to the Raspberry Pi in [this repository's Issues tab](https://gitlab.com/ubports/community-ports/raspberrypi/issues/).


## How do I help make Ubuntu Touch on the Raspberry Pi better?

Installing Ubuntu Touch on your Raspberry Pi then reporting the issues you find here is a great first step! If you're not sure if an issue belongs here or somewhere on GitHub, you can contact us via any of our standard chats or forums:

* [UBports Forum](https://forums.ubports.com/category/43/ut-for-raspberry-pi)
* `#ubports` on freenode IRC
* `#ubports:matrix.org` using Riot, FluffyChat, or your favorite Matrix client
* `@ubports` on Telegram

**Approaches for solving bugs**

**Hardware problem:**

Download Rasbian (or other working Linux OS for the Pi) and check what the difference is.
The major difference between Raspbian and Ubuntu Touch are that Ubuntu Touch uses upstart and mir server instead of systemd and a different display server. Besides that, there could be newer packages in Raspbian which could be a cause of the difference in hardware support.
Both versions use the same kernel.
Use dmesg or logs from unity8.

**Software problem:**

Graphical and performance issues are probably related to Unity 8 which is the desktop/tablet/phone environment.
If it is a rendering problem, than it could also be mesa or mir server.